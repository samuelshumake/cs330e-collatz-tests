#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10000000000000000000000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10000000000000000000000)

    def test_read_without_newline(self):
        s = "1 4"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 4)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_heaviest_nominal(self):
        v = collatz_eval(2, 999999)
        self.assertEqual(v, 525)

    def test_eval_heaviest_projected(self):
        v = collatz_eval(789002, 999999)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 1\n2 3\n1 3\n8 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 3 8\n1 3 8\n8 1 17\n")

    def test_solve_no_cache_effect(self):
        r = StringIO("2 1999\n998002 999999\n999002 1000999\n788002 789999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 1999 182\n998002 999999 396\n999002 1000999 427\n788002 789999 406\n")

    def test_solve_heavy_load(self):
        r = StringIO("1 1000000\n2 999999\n789002 999999\n2 789999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1000000 525\n2 999999 525\n789002 999999 525\n2 789999 509\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
............
----------------------------------------------------------------------
Ran 12 tests in 1.330s

OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
............
----------------------------------------------------------------------
Ran 12 tests in 1.330s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          42      0     14      0   100%
TestCollatz.py      56      0      0      0   100%
------------------------------------------------------------
TOTAL               98      0     14      0   100%
"""
